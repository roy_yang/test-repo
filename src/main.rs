use rand::prelude::*;

pub fn get_rand_0_to_1() -> f32 {
	let mut rng = thread_rng();
    rng.gen()
}

#[cfg(test)]
mod tests {
	use crate::get_rand_0_to_1;
    #[test]
    fn test_works() {
		let x = get_rand_0_to_1();
		assert!( x >= 0.0 && x <= 1.0 );
	}
}

//test comment 1
//test comment 2


fn main() {
	// make a handle to the thread-local generator:
    // random number in range [0, 1)
    let x = get_rand_0_to_1();
    println!("Hello there {}.", x);
}
